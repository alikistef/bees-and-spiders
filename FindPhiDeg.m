function [phideg,out_of_range,camera] = FindPhiDeg(phideg,camera,T,observe2,Dmin,Dmax,out_of_range,N,M)

    omega = 72;
    

    for (i=1:N)
        for (j=1:M)

            if (phideg(i,j) == Inf ) % out of range based on floorplan
                
                continue;
                
            else
            
               % if ( (((phideg(i,j) >= 0) || phideg(i,j) <= omega/2)) && ((observe2 >= Dmin) || (observe2 <= Dmax)) ) % phideg needs to be changed to min angles
                if phideg(i,j) >= 0
                    if phideg(i,j) <= omega/2
                        
                        if observe2(i,j) >= Dmin
                            
                            if observe2(i,j) <= Dmax
                                    phi(i,j) = acos(dot(camera(i,:),T(j,:))/( norm(camera(i,:))*norm(T(j,:))));
                                    phideg(i,j) = rad2deg(phi(i,j));
                                    observe2(i,j) = norm(T(i,:))*cos(phi(i,j));
                            else
                                    %out_of_range(i,j) = 1;
                                    camera = ChangeOrientation(camera,i);
                                    phi(i,j) = acos(dot(camera(i,:),T(j,:))/( norm(camera(i,:))*norm(T(j,:))));
                                    phideg(i,j) = rad2deg(phi(i,j));
                                    observe2(i,j) = norm(T(i,:))*cos(phi(i,j));
                            end
                        else
%                             out_of_range(i,j) = 1;
%                             phideg(i,j) = Inf;
                            camera = ChangeOrientation(camera,i);
                            phi(i,j) = acos(dot(camera(i,:),T(j,:))/( norm(camera(i,:))*norm(T(j,:))));
                            phideg(i,j) = rad2deg(phi(i,j));
                            observe2(i,j) = norm(T(i,:))*cos(phi(i,j));
                        end
                    else
%                         out_of_range(i,j) = 1;
%                         phideg(i,j) = Inf;
                            camera = ChangeOrientation(camera,i);
                            phi(i,j) = acos(dot(camera(i,:),T(j,:))/( norm(camera(i,:))*norm(T(j,:))));
                            phideg(i,j) = rad2deg(phi(i,j));
                            observe2(i,j) = norm(T(i,:))*cos(phi(i,j));
                    end
                else
%                     out_of_range(i,j) = 1;
%                    phideg(i,j) = Inf;
                        camera = ChangeOrientation(camera,i);
                        phi(i,j) = acos(dot(camera(i,:),T(j,:))/( norm(camera(i,:))*norm(T(j,:))));
                        phideg(i,j) = rad2deg(phi(i,j));
                        observe2(i,j) = norm(T(i,:))*cos(phi(i,j));
                end
              
            end
       
        end  
    end
    %[phideg,out_of_range,camera] = InRangeOrNotB(phideg,observe2,out_of_range,camera,N,M);
  
    phideg(phideg(1:N,1:M) == 0 ) = Inf;
end